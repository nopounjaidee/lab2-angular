const express = require("express");
const bodyparser = require("body-parser");
const app = express();
const FeedbackModel = require('./emp_schema');
require('./db')
app.use(bodyparser.json());
app.use(bodyparser.urlencoded());

// alow client to cross domain or ip-address
app.use(function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    //res.setHeader('Access-Control-Allow-Origin','http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods','GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers','content-type, x-access-token');
    res.setHeader('Access-Control-Allow-Credentials','*');
    next();
});


app.get('/',(req,res) =>{
    res.end("rootpart");
});

app.get('/home',(req,res) =>{
    res.end("Home ");
});

app.get('/api',(req,res) =>{
    FeedbackModel.find ((err,doc) =>{
        if (err) res.json({"result":"Failed" });
        res.json({"result":"success", data:doc });
    }) 
}); 

app.post('/api',(req,res) =>{
    // รับค่าจาก bodt pars จาก posman ID "feedback"
    const username = req.body.username;
    const password = req.body.password;
    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    FeedbackModel.create(req.body,(err,doc) =>{
        if (err) res.json({"result":"Failed",username: username,password: passwords, firstname: firstname , lastname : lastname });
    res.json({"result":"success",username: username,password: password, firstname: firstname , lastname : lastname });

    })
    //console.log(res);
    //res.end( "username"+ username +"feedback " + feedback );
});

/*app.post('/api',(req,res) =>{
    // รับค่าจาก bodt pars จาก posman ID "feedback"
    const username = req.body.username;
    const feedback = req.body.feedback;
    res.json({"result":"success",username: username, feedback: feedback});
    //console.log(res);
    //res.end( "username"+ username +"feedback " + feedback );
})*/


app.listen(3000,() => {
    console.log("server Is Running......");
});