const mongoose = require('mongoose');

mongoose.connect('mongodb://96.30.124.238:27017/lab1',{useNewUrlParser: true})

mongoose.connection.on('connected', function (){
    console.log('Mongoose default connection open');
});
mongoose.connection.on('error', function (err){
    console.log('Mongoose default connection err' + err);
});
mongoose.connection.on('disconnected', function (){
    console.log('Mongoose default connection disconnected');
});

process.on('SIGINT',function(){
    mongoose.connection.close(function (){
        console.log('Mongoose default connection disconnected through app trminaltion');
        process.exit(0);
    });
});
