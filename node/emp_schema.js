var mongoose = require ('mongoose');
var tbname = 'nglabemp';

var FeedbackSchema = mongoose.Schema({
    username : {type:String, require:true},
    firstname : {type:String, require:true},
    lastname : {type:String, require:true},
    password : {type:String, require:true}
});

var FeedbackModel = mongoose.model('nglabemp',FeedbackSchema,tbname);
module.exports = FeedbackModel;