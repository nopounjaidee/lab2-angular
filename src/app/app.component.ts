import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

// 
export class AppComponent implements OnInit {
  
  title = 'lam dev angularlab ภาษาไทย';
  mDataArray:any=[]

  constructor(private http:HttpClient){

  }
  onSubmit(feedbackData){
   // alert(JSON.stringify(feedbackData));
    //let postData ={"username":feedbackData.username,"feedback":feedbackData.feedback}
    this.http.post<any>('http://localhost:3000/api', feedbackData).subscribe(result => {
      alert(JSON.stringify(result));
      this.getfeedback();
    });
  }

  getfeedback(){
    this.http.get<any>('http://localhost:3000/api').subscribe(result=>{
      //alert(JSON.stringify(result));
      this.mDataArray = result.data;
    });
  }
  //อธิบาย Method นี้จะทำการ ส่ง data ไม่ที่ URL (คือ ค่า req ฝั่ง api นั้นเอง ) เเละ รอ ฟังหรือการตอบกลับ res จาก API กลับมา
  ngOnInit(): void {
    this.getfeedback();
  }

  
}
// NG สร้างให้มีแค่นี้ 
/*export class AppComponent {
  title = 'lam dev angularlab ภาษาไทย';

}*/
